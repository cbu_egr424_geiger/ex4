# define class Person
# Define an instance variable called age with attr_reader
# Define another instance variable called name with attr_accessor

#class Person 
  attr_reader :age # read only
  attr_accessor :name # read-write

  # Constructor 
  def initialize (name, ageVar)  
    @name = name
    self.age =ageVar
  end 
  
  # Define our own setter method for age
  def age= (new_age) 
    puts "age= method is called"
    @age ||= 5
    @age = new_age if new_age <= 120
  end 

#end 

# After implementing above, uncomment the following and run
person1 = Person.new("Kim", 130) # => 13 
puts "My age is #{person1.age}" # => My age is 13 
person1.age = 13 # Try to change the age 
puts person1.age # => 13 (The setter didn't allow the change)