# define class Person
class Person 
  
  # "CONSTRUCTOR"
  def initialize (name, age)  
    @name = name 
    @age = age 
  end
  
  # getter
  def name
    @name
  end
  
  #gette
  def age
    @age
  end
  
  # setter
  def name= (new_name)
    @name = new_name
  end
end 

# create an instance called person1
person1 = Person.new("Gunnar", 23)

# Output person1's name
puts person1.name 

# Set person1's name to Mikie
person1.name = "Collin"

# Output person1's name
puts person1.name 

# Try to call person1's age
# below will give you undefined method age error
puts person1.age 
