# define class Person 
# with attr_accessor for two instance variables name age
class Person 
    def initialize(name, age)
        @name = name
        @age = age
    end
    
    def get_info
        @additional_info = "Interesting"
        "Name: #{@name}, age: #{@age}"
    end
    
    attr_accessor :name, :age
end 

# Create a new instance person1
person1 = Person.new("Joe", 14)
p person1.instance_variables
puts person1.get_info

# inspect person1's name
p person1.name 
# set person's name to Mike and age to 15
person1.name = "Jacob"
person1.age = 22

# inspect person1
p person1

# set person1's age to string value
person1.age = "twenty-two"

# output person1's age
puts person1.age
