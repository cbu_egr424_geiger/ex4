# define class Person

class Person
  # define constructor
  def initialize
  end
  
  # define get_info method
  def get_info
  end
end

# instantiate person1 object 
#person1 =

# inspect person1's instance variables
#p 

# call person1's get_info
#puts

# inspect person1's instance variables again
# You can see additional_info instance variable got added!
#p 

