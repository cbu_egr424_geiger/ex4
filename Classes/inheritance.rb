# given the definition of class Dog
class Dog 
  def to_s 
    "Dog" 
  end 
  def bark 
    "barks loudly" 
  end 
end 

# define SmallDog class that inherits from Dog
class SmallDog < Dog
  def bark
    "barks softly"
  end
end

# Create instance of dog 
# (btw, new is a class method) 
dog = Dog.new 

# Create instance of small dog
# (btw, new is a class method) 
small_dog = SmallDog.new 

# Uncomment below and check the result
puts "#{dog}1 #{dog.bark}" # => Dog1 barks loudly 
puts "#{small_dog}2 #{small_dog.bark}" # => Dog2 barks quietly 
