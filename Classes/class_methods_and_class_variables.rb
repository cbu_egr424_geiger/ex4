# define class methods
class MathFunctions 
  #define class method double using self. (Static, first way)
  def self.double(var)
    times_called
    var * 2
  end
  
  #define class method times_called using << self (Static, second way)
  class << self 
    def times_called
      @@times_called ||= 0
      @@times_called += 1
    end
  end
  
  #regular method
  def hello_world
    "hello_world"
  end
end 

# define class method called triple under MathFunctions class (Static, third way)
def MathFunctions.triple (var)
  times_called
  var * 3
end

# No instance created! 
puts MathFunctions.double 5 # => 10 
puts MathFunctions.triple(3) # => 9 
puts MathFunctions.times_called # => 3 

# If you need to call hello_world, you need to instantiate an object and call the method on the object
o1 = MathFunctions.new
puts o1.hello_world
