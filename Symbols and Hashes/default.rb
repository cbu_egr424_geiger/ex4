# Create Hash word_frequency with default value 0 for non-existing entries
word_frequency = Hash.new(0)

sentence = "Chicka chicka boom BOOM Boom" 

# populate word_frequency hash table with each word as key and the frequency as value
# you need to use downcase string API
sentence.split.each do | word |
    word_frequency[word.downcase] += 1
end

# inspect word_frequency 
p word_frequency 
