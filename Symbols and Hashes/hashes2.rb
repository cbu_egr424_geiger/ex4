# define hash family_tree_19 with symbol :oldest :older :younger
# using symbol: syntax
family_tree_19 = {oldest:"Jim", older:"Joe", younger:"Jack"}

p family_tree_19

# reterive the value of the entry with key :youngest 
family_tree_19[:youngest] = "Jeremy" 

# inspect family_tree_19
p family_tree_19 

# Named parameter "like" behavior... 
# define adjust_colors method with some default parameter that takes in a hash as the argument
def adjust_colors (props = {foreground: "red", background: "white"}) 
  puts  "Foreground: #{props[:foreground]}" if props[:foreground]
  puts  "Background: #{props[:background]}" if props[:background]
end

# call adjust_colors without any argument
adjust_colors 

# call adjust_colors with different hash with different syntax
adjust_colors ({:foreground => "green"}) 
adjust_colors background: "yella" 
adjust_colors :background => "magenta"
