# Assign range from 1-3 to some_range (use two dots)
some_range = 1..3

# Output the max of some_range
p some_range
puts some_range.max

# Output whether some_range includes 2
puts some_range.include? 2 

# Output whether a range from 1~10 includes 5.3 using triple equal
puts (1..10) === 5.3

# Output whether 5.3 includes a range from 1~10 using triple equal 
puts 5.3 === (1..10)

# Ranges with three dots are end-exclusive
puts ('a'...'r') === 'r'

# Convert ranges from 'k' to 'z' to array and randomly pick any two elements
p ('k'..'z').to_a.sample(2)

# Fill in the following case statement
age = 55 
case age 
  when 0..12 then puts "Still a baby" 
  when 13..99 then puts "Teenager at heart"
  else  puts "You are getting older"
end 
