het_arr = [1, "two", :three] # heterogeneous types 

# type code to get the second element
puts het_arr[1] 

# type code to set array of strings using %w
arr_words = %w{ what a great day today! }
p arr_words

# type code to get day using negative index
puts arr_words[-2]

# type code to print what - today! using string interpolation
puts "#{arr_words.first} - #{arr_words.last}"

# p foo is the same as p foo.inspect
# p outputs more details (useful for debugging)

# Type the correct subscript to get ["great", "day"]
p arr_words[-3 , 2] # go back 3 and get 2

# get a list of string ["great", "day", "today"] using Range
p arr_words[2..4] #

# Make a String out of array elements separated by ‘,’
puts arr_words.join(',') 