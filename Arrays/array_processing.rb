a = [1, 3, 4, 7, 8, 10] 

# Print each element in a with a newline
a.each { |n| puts n }

# Select all elements which is greater than 4 and assign such elements to a new array new_arr
new_arr = a.select { |n| n > 4 }

# Inspect new_arr
p new_arr 

# Select all elements which is smaller than 10 but reject even numbers and assign those elements to a new array new_arr
new_arr = a.select { |n| n < 10 }.reject { |n| n.even? }

# Inspect new_arr
p new_arr 


# Multiply each element by 3 producing new array
new_arr = a.map {|x| x * 3} 

# Inspect new_arr
p new_arr 

# Inspect a
p a

# Use map! instead
a.map! {|x| x * 3} 

# Inspect a
p a


