# You want a stack (LIFO)? Sure 
stack = []

# Write code to add "one" using <<
stack << "one" 

# Write code to add "two" using push 
stack.push("two")

# Write code to print the last element
puts stack.pop

# You need a queue (FIFO)? We have those too... 
queue = []; 

# Write code to add "one" and "two" in a single line
queue.push("one") ; queue.push("two")

# call queue's shift method and output
puts queue.shift  

# inspect queue to see the elements
p queue

# assign 5,3,4,2 in array a  
a = [5,3,4,2]

# call sort! and reverse! method together to have a changed permanently 
a.sort!.reverse!

# inspect a
p a 

# pick a random sample from a 
p a.sample(2)

# assign 33 to index 6
a[6] = 33

# inspect a
p a 
