class Foo
 def public_m
  private_m 
 end
 private
 def private_m
  puts 'Hello'
 end
end

# Why does it not work? How to make it work?
Foo.new.public_m
